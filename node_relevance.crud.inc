<?php

/**
 * The current relevance of the node for the user in the given context.
 * 
 * @param $uid the user for which the relevance of the node is measured
 * @param $nid the ID of the node for which the relevance is measured
 * @param $context the context in which the node is relevant to the user
 * @return the numeric value of the relevance
 */
function node_relevance_get($uid, $nid, $context) {
  return node_relevance_op('get', $uid, $nid, $context);
}

/**
 * Sets the current relevance of the node for the user in the given context.
 * 
 * @param $uid the user for which the relevance of the node is measured
 * @param $nid the ID of the node for which the relevance is measured
 * @param $context the context in which the node is relevant to the user
 * @param $value the value of the relevance
 */
function node_relevance_set($uid, $nid, $context, $value) {
  node_relevance_op('set', $uid, $nid, $context, $value);
}

/**
 * Updates the current relevance of the node for the user in the given context.
 * 
 * @param $uid the user for which the relevance of the node is measured
 * @param $nid the ID of the node for which the relevance is measured
 * @param $context the context in which the node is relevant to the user
 * @param $value the value with which to update the relevance
 */
function node_relevance_update($uid, $nid, $context, $value) {
  node_relevance_op('update', $uid, $nid, $context, $value);
}

/**
 * Deletes a single node relevance statistic.
 * 
 * @param $uid the user for which the relevance of the node is measured
 * @param $nid the ID of the node for which the relevance is measured
 * @param $context the context in which the node is relevant to the user
 */
function node_relevance_delete($uid, $nid, $context) {
  node_relevance_op('del', $uid, $nid, $context, $value);
}

/**
 * Deletes all matching node relevance statistics.
 * 
 * @param $uid the user for which the relevance of the node is measured
 * @param $nid the ID of the node for which the relevance is measured
 * @param $context the context in which the node is relevant to the user
 */
function node_relevance_delete_all($uid, $nid, $context) {
  node_relevance_op('del_matching', $uid, $nid, $context, $value);
}

/**
 * Perform an operation on node relevances.
 */
function node_relevance_op($op, $uid, $nid, $context, $value = 0) {
  static $cache;
  
  switch($op) {
    case 'get':
      // Check if result is cached
      if (isset($cache[$context][$uid . '_' . $nid]))
        return $cache[$context][$uid . '_' . $nid];
        
      // Otherwise, issue a DB query
      $relevance = db_result(db_query("SELECT relevance FROM {node_relevance} " .
                                      "WHERE uid = %d AND nid = %d AND context = '%s'",
                                      $uid, $nid, $context));
                                      
      // Cache the result. 
      // TODO: how to distinguish between db failure and 0 relevancy?
      $cache[$context][$uid . '_' . $nid] = $relevance;
      
      return $relevance;
      break;
      
    case 'set':
    case 'update':
      // Figure out what kind of SQL update we're doing
      $relevance_sql = ($op == 'set' ? 
                        "relevance = %d" : 
                        "relevance = relevance + %d");
      
      // Issue a db query
      $success = db_query("UPDATE {node_relevance} SET $relevance_sql " .
                          "WHERE uid = %d AND nid = %d AND context = '%s'",
                          $value, $uid, $nid, $context);
                          
      // Sanity check
      if (!$success) {
        return;
      }
      
      // If there were affected rows, it means that a row already existed
      // and it was updated. We can cache the result and stop right now.
      if (db_affected_rows()) {
        $cache[$context][$uid . '_' . $nid] = $value;
        return;
      }
      
      // If we reached this point, we create a new row to record the relevance
      $entry = array('uid' => $uid, 'nid' => $nid, 'context' => $context, 
                     'relevance' => $value);
      
      // Write the new relevance to the db 
      $success = drupal_write_record('node_relevance', $entry);
     
      // Check for success
      if (!$success)
        return;
      
      // Cache the result
      $cache[$context][$uid . '_' . $nid] = $value;
      break;
      
    case 'del':
    case 'del_matching':
      // Make sure that at least one criteria is specified
      if (!isset($uid) && !isset($nid) && !isset($context))
        return;
        
      $conditions = array(); $values = array();
      
      if (isset($uid)) {
        $conditions []= 'uid = %d';
        $values []= $uid;
      }
      
      if (isset($nid)) {
        $conditions []= 'nid = %d';
        $values []= $nid;
      }
      
      if (isset($context)) {
        $conditions []= "context = '%s'";
        $values []= $context;
      }
      
      // Build the query, using the parameters which are present
      $query = "DELETE FROM {node_relevance} WHERE " . 
               implode($conditions, ' AND ');
               
      // Delete all matching node relevance statistics
      $success = db_query($query, $values);
      
      // If the SQL query was successful, update the local cache
      if ($success) {
        // It's easier to trash the whole cache at this point
        if ($op == 'del_matching')
          $cache = array();
        else
          unset($cache[$context][$uid . '_' . $nid]);
      }
      break;
  }
}

/**
 * Create a new context
 * 
 * @param $machine_name the machine name of the context
 * @param $name the human-readable name of the context
 */
function node_relevance_create_context($machine_name, $name) {
  $context = array('context' => $machine_name, 'name' => $name);
  drupal_write_record('node_relevance_contexts', $context);
}

/**
 * Deletes a given context
 * 
 * @param $machine_name the machine name of the context
 */
function node_relevance_delete_context($machine_name) {
  db_query("DELETE FROM {node_relevance_contexts} WHERE context = '%s'", 
           $machine_name);
}

/**
 * Returns a list of all contexts in which a node can be relevant to a user
 */
function node_relevance_all_contexts() {
  $contexts = array();
  
  $result = db_query("SELECT context, name FROM {node_relevance_contexts}");
  while ($context = db_fetch_array($result)) {
    $contexts[$context['context']] = $context;
  }
  
  return $contexts;
}
