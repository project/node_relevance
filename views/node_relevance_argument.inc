<?php

class node_relevance_argument extends views_handler_argument_numeric {
 
  function options_form(&$form, &$form_state) {
    $options = parent::options_form($form, $form_state);
    
    // Retrieve relevance contexts
    $contexts = node_relevance_all_contexts();
    foreach ($contexts as $id => $context) {
      $options[$id] = $context['name'];
    }
    
    $form['context'] = array(
      '#type' => 'select',
      '#title' => 'Relevance context',
      '#description' => 'The relevance context for which we are viewing statistics',
      '#options' => $options,
      '#required' => TRUE,
    );
  }
  
  function query() {
    parent::query();

    // If we don't have a context configured, there's nothing more to do
    if (empty($this->options['context']))
      return;
      
    $alias = $this->ensure_my_table();
    
    // Add a new condition for the join: context is the one configured
    $this->query->table_queue[$alias]['join']->definition['extra'] []= array(
      'field' => 'context',
      'value' => $this->options['context'],
      'numeric' => FALSE,
    );

    // Re-construct the join, because we added new info to it
    $this->query->table_queue[$alias]['join']->construct();
    $this->query->table_queue[$alias]['join']->adjusted = TRUE;
  }
}