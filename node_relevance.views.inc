<?php

/**
 * Implementation of hook_views_data().
 * 
 * We use it to describe our {node_relevance} table to Views.
 */
function node_relevance_views_data() {
  $data = array();
  
  $data['node_relevance']['table']['group'] = t('Node Relevance');
  
  $data['node_relevance']['table']['join'] = array(
    'users' => array(
      'field' => 'uid',
      'table' => 'node_relevance',
      'left_field' => 'uid',
      'left_table' => 'users',
    ),
    'node' => array(
      'field' => 'nid',
      'table' => 'node_relevance',
      'left_field' => 'nid',
      'left_table' => 'node',
    ),
    'node_relevance_contexts' => array(
      'field' => 'context',
      'left_field' => 'context',
    ),
  );

  $data['node_relevance']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user for which this statistic is relevant'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'relationship table' => 'node_relevance',
      'relationship field' => 'uid',
      'label' => t('user'),
    ),
    'argument' => array(
      'title' => t('Nodes relevant for user'),
      'handler' => 'node_relevance_argument',
      'help' => t('Filters selected nodes so that only those that are relevant '.
                  'to the user from URL in the selected context are displayed'),
    ),
  );
  
  $data['node_relevance']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The node which is relevant in the given context'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'relationship table' => 'node_relevance',
      'relationship field' => 'nid',
      'label' => t('node')
    ),
    'argument' => array(
      'title' => t('Users for which node is relevant'),
      'handler' => 'node_relevance_argument',
      'table' => 'node_relevance',
      'help' => t('Filters selected users so that only for which the node '.
                  'from url is relevant in the selected context are displayed'),
    ),
  );
  
  $data['node_relevance']['context'] = array(
    'title' => t('Context'),
    'help' => t('The context in which the node is relevant to the user'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  $data['node_relevance']['relevance'] = array(
    'title' => t('Relevance'),
    'help' => t('Numeric value of the relevance of the node to the user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function node_relevance_views_data_alter(&$data) {
  $data['users']['table']['join']['node_relevance'] = array(
    'table' => 'users',
    'field' => 'uid',
    'left_table' => 'node_relevance',
    'left_field' => 'uid',
  );
  
  $data['node']['table']['join']['node_relevance'] = array(
    'table' => 'node',
    'field' => 'nid',
    'left_table' => 'node_relevance',
    'left_field' => 'nid',
  );
  
  $data['users']['node_relevance'] = array(
    'title' => t('Relevance Statistics'),
    'help' => t('Node relevance statistics for this user'),
    'real field' => 'uid',
    'relationship' => array(
      'handler' => 'node_relevance_relationship',
      'base' => 'node_relevance',
      'base field' => 'uid',
      'relationship table' => 'users',
      'relationship field' => 'uid',
      'label' => t('relevance statistics'),
    ),
  );
  
  $data['node']['node_relevance'] = array(
    'title' => t('Relevance Statistics'),
    'help' => t('User relevance statistics for this node'),
    'real field' => 'nid',
    'relationship' => array(
      'handler' => 'node_relevance_relationship',
      'base' => 'node_relevance',
      'base field' => 'nid',
      'relationship table' => 'node',
      'relationship field' => 'nid',
      'label' => t('relevance statistics'),
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function node_relevance_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'node_relevance') . '/views',
    ),
    'handlers' => array(
      'node_relevance_relationship' => array(
        'parent' => 'views_handler_relationship',
      ),
      'node_relevance_argument' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
    ),
  );
}
